<?php /*
 Template Name: Contactez-Nous
 */ ?>
<?php get_header()?>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<section class="breadcrumb_area" style="background: url(<?php echo get_template_directory_uri();?>/assets/img/banner_bg_six.jpg);">
        <div class="overlay_bg"></div>
        <div class="container">
            <div class="breadcrumb_content text-center">
                <h1>Contactez-Nous</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Acceuil</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Contactez-Nous</li>
                    </ol>
                </nav>
            </div> 
        </div>
</section>
    
    <section class="contact_area_two sec_pad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="get_info">
                        <h3>Nous sommes toujours ouverts pour vous</h3>
                        
                        <div class="info_item">
                            <i class="ti-location-pin"></i>
                            <h6>Address:</h6>
                            <p>424 Mill <br> MA 02474, Paris</p>
                        </div>
                        <div class="info_item">
                            <i class="ti-mobile"></i>
                            <h6>Phone:</h6>
                            <p><a href="tel:948256347968">0988290198</a></p>
                        </div>
                        <div class="info_item">
                            <i class="ti-email"></i>
                            <h6>Email:</h6>
                            <p><a href="mailto:picme@gmail.com">CabParis@gmail.com</a></p>
                        </div>
                        <div class="social_icon">
                            <h6>Connect With Us</h6>
                            <ul class="list-unstyled">
                                <li><a href="#"><i class="ti-facebook"></i></a></li>
                                <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                                <li><a href="#"><i class="ti-vimeo-alt"></i></a></li>
                                <li><a href="#"><i class="ti-pinterest"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-6">
                    <div class="contact_info_two">
                        <div class="section_title">
                            <h5>COMMENT POUVONS-NOUS VOUS AIDER?</h5>
                            <h2>Avez-vous une question?</h2>
                        </div>
                        <?php echo do_shortcode("[ninja_form id='1']") ?>
                        <!--
                        <form  action="#" method="post" id="contactForm" novalidate="novalidate" class="contact_form">
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Nom">
                                <label class="border_line"></label>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                <label class="border_line"></label>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="subject" name="subject" placeholder="Sujet">
                                <label class="border_line"></label>
                            </div>
                            <div class="form-group">
                                <textarea id="message" name="message" cols="30" rows="10" class="form-control" placeholder="Votre Message"></textarea>
                                <label class="border_line"></label>
                            </div>
                            <div class="g-recaptcha" data-sitekey="6LdbKickAAAAAOmnpKMqtRpKKXGhaUXOWvn-CllT"></div>
                             <div class="form-group">
                                <button type="submit" value="submit" class="slider_btn yellow_hover">Envoyer Message</button>
                             </div>
                        </form>
-->
                    </div>
                </div>
            </div>
        </div>
    </section>
   
<?php get_footer()?>