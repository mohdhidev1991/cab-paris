<?php /*
 Template Name: get taxi
 */ ?>
<?php get_header()?>
<section class="breadcrumb_area" style="background: url(<?php echo get_template_directory_uri();?>/assets/img/banner_bg_four.jpg);">
        <div class="overlay_bg"></div>
        <div class="container">
            <div class="breadcrumb_content text-center">
                <h1>Demander un devis en ligne</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Acceuil</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Demander un devis</li>
                    </ol>
                </nav>
            </div> 
        </div>
    </section>
    
    <section class="booking_form_area_two sec_pad bg_one">
        <div class="container">
            <div class="booking_slider slick">
            <div class="booking_form_info">
                <div class="tab_img">
                    <div class="b_overlay_bg"></div>
                    <img src="<?php echo get_template_directory_uri();?>/assets/img/pngegg.png" width="100%" alt="">
                </div>
                <div class="boking_content">
                    <h2>Réservation Taxi Roissy</h2>
                    <form action="#" class="row booking_form">
                        <div class="col-md-12">
                            <div class="form-group choose_item">
                                <label>
                                    <input type="radio" value="standard" name="radio-group" checked>
                                    <span>Van</span>
                                </label>
                                <label>
                                    <input type="radio" value="business" name="radio-group" >
                                    <span>Berline</span>
                                </label>
                                
                                <label>
                                    <input type="radio" value="bus" name="radio-group">
                                    <span>Luxe</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="&#xe08a  Your Name">
                                <label class="border_line"></label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="number" class="form-control" placeholder="&#xe090  Phone">
                                <label class="border_line"></label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="&#xe01d  Start Destination">
                                <label class="border_line"></label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="&#xe01d  End Destination">
                                <label class="border_line"></label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control date-input-css" placeholder="&#xe06b  Time and Date">
                                <label class="border_line"></label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="&#xe0db;  Car Type">
                                <label class="border_line"></label>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <a href="#" class="btn slider_btn dark_hover">Demander un devis</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="booking_form_info two">
                <div class="tab_img">
                    <div class="b_overlay_bg"></div>
                    <img src="<?php echo get_template_directory_uri();?>/assets/img/booking_car.png" width="100%" alt="">
                </div>
                <div class="boking_content">
                    <h2>Réservation Taxi Orly</h2>
                    <form action="#" class="row booking_form">
                        <div class="col-md-12">
                            <div class="form-group choose_item">
                                <label>
                                    <input type="radio" value="standard" name="radio-group">
                                    <span>Van</span>
                                </label>
                                <label>
                                    <input type="radio" value="business" name="radio-group" >
                                    <span>Berline</span>
                                </label>
                                
                                <label>
                                    <input type="radio" value="bus" name="radio-group" checked>
                                    <span>Luxe</span>
                                </label>
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="&#xe08a  Your Name">
                                <label class="border_line"></label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="number" class="form-control" placeholder="&#xe090  Phone">
                                <label class="border_line"></label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="&#xe01d  Start Destination">
                                <label class="border_line"></label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="&#xe01d  End Destination">
                                <label class="border_line"></label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control date-input-css" placeholder="&#xe06b  Time and Date">
                                <label class="border_line"></label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="&#xe0db;  Car Type">
                                <label class="border_line"></label>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <a href="#" class="btn slider_btn dark_hover">Demander un devis</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            </div>
        </div>
    </div>
    </section>
    
   

<?php get_footer()?>