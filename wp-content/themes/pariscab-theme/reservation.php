<?php

/**
 * Cap Paris Sample.
 *
 * This file adds the landing page template to the Genesis Sample Theme.
 *
 * Template Name: ReservationPage
 *
 **/

 ?>


<?php get_header()?>

<section class="booking_form_area bg_one" style="margin-top: 10%;z-index: -1">
        <div class="container">
            <div class="booking_slider slick">
                <div class="booking_form_info">
                    <div class="tab_img">
                        <div class="b_overlay_bg"></div>
                        <img src="<?php echo get_template_directory_uri();?>/assets/img/booking_car.png" width="100%" alt="">
                    </div>
                    <div class="boking_content" id="tour">
                        

                        
                        <form  class="row booking_form">

                            <script>
                            function resizeIframe(obj) {
                                obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px';
                            }
                            </script>
                            <div class="col-md-12">
                            <iframe src="https://proxijet.fr/site/reservation.php?secure=NDUwLTUzNA==" frameborder="0" scrolling="no" onload="resizeIframe(this)" style="height:1000px;width:600px;border:none;overflow:hidden;" width="100%" height="1300px" marginheight="0" frameborder="0" border="0"></iframe>
                            </div>
                            
                        </form>
                        
                    </div>
                </div>
                <div class="booking_form_info two">
                    <div class="tab_img">
                        <div class="b_overlay_bg"></div>
                        <img src="<?php echo get_template_directory_uri();?>/assets/img/booking_car.png" width="100%" alt="">
                    </div>
                    <div class="boking_content">
                        <form action="util.php" method="POST" class="row booking_form">

                            <script>
                            function resizeIframe(obj) {
                                obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 'px';
                            }
                            </script>
                            <div class="col-md-12">
                            <iframe src="https://proxijet.fr/site/reservation.php?secure=NDUwLTUzNA==" frameborder="0" scrolling="no" onload="resizeIframe(this)" style="height:1000px;width:600px;border:none;overflow:hidden;" width="100%" height="1300px" marginheight="0" frameborder="0" border="0"></iframe>
                            </div>
                        </form>
                        
                    </div>
                </div>

            </div>
        </div>
        </div>
    </section>
    
    <?php get_footer()?>