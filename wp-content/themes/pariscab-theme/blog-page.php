<?php

/**
 * Cap Paris Sample.
 *
 * This file adds the landing page template to the Genesis Sample Theme.
 *
 * Template Name: BlogPage
 *
 **/

 ?>


<?php get_header()?>

<section class="breadcrumb_area" style="background: url(<?php echo get_template_directory_uri();?>/assets/img/banner_bg_three.jpg);">
        <div class="overlay_bg"></div>
        <div class="container">
            <div class="breadcrumb_content text-center">
                <h1>Blog</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Accueil</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Blog</li>
                    </ol>
                </nav>
            </div> 
        </div>
    </section>
    <section class="blog_area sec_pad">
        <div class="container">
            <div class="row blog_inner">
                <div class="col-lg-12">
                    <div class="main_blog_inner">

                        <?php
                            $the_query = new WP_Query(array("post_type" => "Post", "orderby" => "date", "order" => "ASC" , 'posts_per_page' => '8' ));
                        ?>
                        <?php if ($the_query->have_posts()) { ?>
                        <?php
                            while ($the_query->have_posts()) {
                            $the_query->the_post();
                        ?>
                        <?php 
                            $id_article = get_the_ID();
                            $title = get_the_title();
                            $content = get_the_content();
                            $image = get_field('image');
                            $admin = get_author_name(get_the_author_ID());
                            $date = get_the_date();
                        ?>
                        <div class="blog_item">
                            <div class="blog_img">
                                <a href="#" class="overlay"><img src="<?php echo $image['url'];?>" alt=""></a>
                            </div>
                            <div class="blog_content">
                                <ul class="list-unstyled post_info">
                                    <li><a href="?p=<?php echo $id_article ;?>"><?php echo $date?></a></li>
                                    <li><a href="?p=<?php echo $id_article ;?>"><?php echo $admin?></a></li>
                                </ul>
                                <h2 class="blog_title"><a href="?p=<?php echo $id_article ;?>"><?php echo $title;?></a></h2>
                                
                                <a href="?p=<?php echo $id_article ;?>" class="slider_btn yellow_hover">Lire l'article</a>
                            </div>
                        </div>
                        <?php
                        }
                        wp_reset_postdata();
                        ?>
                        <?php } ?>    
                    </div>
                </div>
                
            </div>
        </div>
    </section>


    
<?php get_footer()?>