<footer class="footer_area sec_pad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="f_widget about_widget">
                        <div class="f_social_icon">
                            <a href="#" class="ti-facebook"></a>
                            <a href="#" class="ti-instagram"></a>
                        </div>
                        <p>Crée et référencé Par :<br>L'agence web marketing Jinfo 2023
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="f_widget link_widget">
                        <h2 class="f_title">Explorer</h2>

                        <?php
                        wp_nav_menu(array(
                        'theme_location' => 'secondry',
                        'container' => false,
                        'items_wrap' => '<ul class="list-unstyled f_list">%3$s</ul>'
                        ));
                        ?>

                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="f_widget link_widget">
                        <h2 class="f_title">Liens Utiles</h2>

                            <?php
                            wp_nav_menu(array(
                            'theme_location' => 'third',
                            'container' => false,
                            'items_wrap' => '<ul class="list-unstyled f_list">%3$s</ul>'
                            ));
                            ?>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="f_widget link_widget">
                        <h2 class="f_title">Besoin d'aide ?</h2>
                        <ul class="list-unstyled f_list">
                            
                            <?php
                            wp_nav_menu(array(
                            'theme_location' => 'four',
                            'container' => false,
                            'items_wrap' => '<ul class="list-unstyled f_list">%3$s</ul>'
                            ));
                            ?>

                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--================Contact Success and Error message Area =================-->
    <div id="success" class="modal modal-message fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></span>
                    <h2 class="modal-title">Thank you</h2>
                    <p class="modal-subtitle">Your message is successfully sent...</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Modals error -->

    <div id="error" class="modal modal-message fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></span>
                    <h2 class="modal-title">Sorry</h2>
                    <p class="modal-subtitle"> Something went wrong </p>
                </div>
            </div>
        </div>
    </div>
    <!--================End Contact Success and Error message Area =================-->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo get_template_directory_uri();?>/assets/js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/vendors/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/vendors/slick/slick.min.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/vendors/calender/dcalendar.picker.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/vendors/bootstrap-datepicker/datepicker.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/js/wow.min.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/js/smoothscroll.js"></script>
    <!-- contact js -->
    <script src="<?php echo get_template_directory_uri();?>/assets/js/jquery.form.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/js/contact.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/assets/js/custom.js"></script>
</body>


<!-- Mirrored from CabParis-html.pixelomatic.com/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 25 Jan 2023 11:24:16 GMT -->

</html>
<?php wp_footer();?>