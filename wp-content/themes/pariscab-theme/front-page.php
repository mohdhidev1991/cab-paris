<?php

/**
 * Cap Paris Sample.
 *
 * This file adds the landing page template to the Genesis Sample Theme.
 *
 * Template Name: AccueilPage
 *
 **/

 ?>


<?php get_header()?>


    <?php
    $the_query = new WP_Query(array("post_type" => "Slider", "orderby" => "date", "order" => "ASC"));
    ?>
    <section class="slider_area d-flex align-items-center">
    <?php if ($the_query->have_posts()) { ?>
        <?php
            while ($the_query->have_posts()) {
            $the_query->the_post();
        ?>
        <?php 
            $image1 = get_field('image1');
            $image2 = get_field('image2');
            $image3 = get_field('image3');
            $title = get_field('title');
            $description = get_field('description');
            $button = get_field('button');
            $lien = get_field('lien');
        ?>
        <div class="ovarlay"></div>
        <div class="background_slider slick">
            <div class="bg_img" style="background: url(<?php echo $image1['url'];?>) center center / cover no-repeat scroll;">
            </div>
            <div class="bg_img" style="background: url(<?php echo $image2['url'];?>) center center / cover no-repeat scroll;">
            </div>
            <div class="bg_img" style="background: url(<?php echo $image3['url'];?>) center center / cover no-repeat scroll;">
            </div>
        </div>
        <div class="container">
            <div class="slider_text text-center">
                <?php echo $title;?>
                <?php echo $description;?>
            </div>
        </div>
        <?php
            }
            wp_reset_postdata();
            ?>
            <?php } ?>
    </section>

    <section class="booking_form_area bg_one" style="margin-top:-280px;z-index: -1">
        <div class="container">
            <div class="booking_slider slick">
                <div class="booking_form_info">
                    <div class="tab_img">
                        <div class="b_overlay_bg"></div>
                        <img src="<?php echo get_template_directory_uri();?>/assets/img/booking_car.png" width="100%" alt="">
                    </div>
                    <div class="boking_content" id="tour">
                            <h1>Réservez votre taxi</h1>
                            <h6>Jusqu'à 15 minutes avant votre départ</h6>
                            <h2>Paris =&gt; Aéroports</h2>
                            <form action="#" class="row booking_form">
                               
                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <a href="/reservation" class="btn slider_btn dark_hover">Réservez maintenant!</a>
                                    </div>
                                </div>
                            </form>
 
                        
                    </div>
                </div>
                <div class="booking_form_info two">
                    <div class="tab_img">
                        <div class="b_overlay_bg"></div>
                        <img src="<?php echo get_template_directory_uri();?>/assets/img/booking_car.png" width="100%" alt="">
                    </div>
                    <div class="boking_content">
                           <h1>Réservez votre taxi</h1>
                            <h6>Jusqu'à 15 minutes avant votre départ</h6>
                            <h2>Paris =&gt; Aéroports</h2>
                            <form action="#" class="row booking_form">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <a href="/reservation" class="btn slider_btn dark_hover">Réservez maintenant!</a>
                                    </div>
                                </div>
                            </form>
                        
                    </div>
                </div>

            </div>
        </div>
        </div>
    </section>


    <section class="featured_area bg_one">
        <div class="container">
            <div class="section_title text-center">
                <h5>Nos Offres</h5>
                <h2>Nous sommes une entreprise de talent</h2>
            </div>
            <?php
                     $the_query = new WP_Query(array("post_type" => "offres", "orderby" => "date", "order" => "ASC"));
                     ?>
            <div class="row featured_info slick">
            <?php if ($the_query->have_posts()) { ?>
                <?php
					while ($the_query->have_posts()) {
					$the_query->the_post();
				?>
            	<?php 
                    $icon = get_field('icon');
					$title = get_field('title');
					$description = get_field('description');
                    $lien = get_field('lien');

				?>    
                <div class="col-lg-12">
                    <div class="featured_item">
                        <i class="<?php echo $icon; ?>"></i>
                        <?php echo $title;?>
                        <?php echo $description;?>
                        
                        <a href="#" class="learn_btn">Learn More <i class="ti-arrow-right"></i></a>
                    </div>
                </div>
                <!--<div class="col-lg-12">
                    <div class="featured_item">
                        <i class="flaticon-hotel icon"></i>
                        <h3>Déplacez-vous où vous voulez</h3>
                        <p>He nicked it spiffing good time lurgy william bonnet haggle crikey ruddy, bits and bobs
                            charles brown bread bugger what.!</p>
                        <a href="#" class="learn_btn">Learn More <i class="ti-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="featured_item">
                        <i class="flaticon-plane icon"></i>
                        <h3>Prise en charge à l'aéroport</h3>
                        <p>He nicked it spiffing good time lurgy william bonnet haggle crikey ruddy, bits and bobs
                            charles brown bread bugger what.!</p>
                        <a href="#" class="learn_btn">Learn More <i class="ti-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="featured_item">
                        <i class="flaticon-hotel icon"></i>
                        <h3>Réservation en direct sur l'hôtel</h3>
                        <p>He nicked it spiffing good time lurgy william bonnet haggle crikey ruddy, bits and bobs
                            charles brown bread bugger what.!</p>
                        <a href="#" class="learn_btn">Savoir plus <i class="ti-arrow-right"></i></a>
                    </div>
                </div>-->   <?php
                        }
                        wp_reset_postdata();
            ?>
            <?php } ?>
            </div>
         
        </div>
    </section>


    <section class="advantage_area bg_one">
        <div class="container">
        <?php
                     $the_query = new WP_Query(array("post_type" => "aventages", "orderby" => "date", "order" => "ASC"));
                     ?>
            <div class="section_title text-center">
                <h5>Caractéristiques principales</h5>
                <h2>Nos avantages</h2>
            </div>

            <div class="row" style="display: flex; align-items: center;">
            <?php if ($the_query->have_posts()) { ?>
                <?php
					while ($the_query->have_posts()) {
					$the_query->the_post();
				?>
            	<?php 
                    $icon1= get_field('icon1');
					$title = get_field('title');
					$description = get_field('description');
                    $image_1 = get_field('image_1');
                    $image_2 = get_field('image_2');
                    ?>
                <div class="col-lg-3 col-md-6">
                    <div class="advantage_item" style="align-items: center;">
                        <i class="<?php echo $icon1?>"></i>
                        <h3><?php echo $title ?></h3>
                        <p><?php echo $description ?></p>
                    </div>
                </div>
                <!--<div class="col-lg-3 col-md-6">
                    <div class="advantage_item">
                        <i class="flaticon-travel"></i>
                        <h3>Plusieurs places a découvrir</h3>
                        <p>Mush some dodgy chav sloshed bubble and squeak brolly owt to do with me bum.</p>
                    </div>
                </div>-->
                <!--<div class="col-lg-3 col-md-6">
                    <div class="advantage_item">
                        <i class="flaticon-car"></i>
                        <h3>Voitures de Luxe</h3>
                        <p>Mush some dodgy chav sloshed bubble and squeak brolly owt to do with me bum.</p>
                    </div>
                </div>-->
                <!--<div class="col-lg-3 col-md-6">
                    <div class="advantage_item">
                        <i class="flaticon-bag"></i>
                        <h3>Autres Services</h3>
                        <p>Mush some dodgy chav sloshed bubble and squeak brolly owt to do with me bum.</p>
                    </div>
                </div>-->
              <?php
                        }
                        wp_reset_postdata();
            ?>
            <?php } ?>  
            </div>  
            <div class="car_img" style="max-height: max-content;">
                <div style="align-items: center; position: relative;">
                    <img class="car_two wow caranimationOne" data-wow-delay="1s" src="<?php echo $image_1['url'] ?>" style="width: 100%;" alt="">
                    <img data-wow-delay="1s" src="<?php echo $image_2['url'] ?>" style="width: 100%;" alt="">
                </div>
                
            </div>
          
        </div>
    </section>
    <section class="call_action_area">
        <div class="container">
        <?php
                     $the_query = new WP_Query(array("post_type" => "Contacter", "orderby" => "date", "order" => "ASC"));
                     ?>
            <div class="row">
            <?php if ($the_query->have_posts()) { ?>
                <?php
					while ($the_query->have_posts()) {
					$the_query->the_post();
				?>
            	<?php 
                    $image= get_field('image');
					$title = get_field('title');
					$description = get_field('description');
                    $lien=get_field('lien');
                    $numero= get_field('numero');
                    ?>
                <div class="col-lg-7">
                    <div class="action_img">
                        <div class="overlay_bg"></div>
                        <img src="<?php echo $image ['url']?>" alt="">
                    </div>
                </div>
                <div class="col-lg-5 d-flex align-items-center">
                    <div class="action_content">
                        <h3><?php echo $title;?></h3>
                        <a href="<?php echo $lien?>" class="call_btn"><?php echo $numero;?></a>
                        <p><?php echo $description ?></p>
                        <a href="<?php echo $lien?>" class="slider_btn dark_hover">Appelez-nous <i
                                class="icon_plus"></i></a>
                    </div>
                </div>
            </div>
            <?php
                        }
                        wp_reset_postdata();
            ?>
            <?php } ?>     
        </div>

    </section>
    <img src="img/div.png" width="100%">
    <section class="testimonila_area sec_pad">
        <div class="overlay_bg"></div>
        <div class="container">
            <div class="section_title text-center">
                <h5>Retour</h5>
                <h2 class="color_w">Ce que disent les clients</h2>
            </div>
            <?php
                     $the_query = new WP_Query(array("post_type" => "temoiniages", "orderby" => "date", "order" => "ASC"));
                     ?>
            <div class="testimonial_slider slick">
            <?php if ($the_query->have_posts()) { ?>
                <?php
					while ($the_query->have_posts()) {
					$the_query->the_post();
				?>
            	<?php 
                    $image= get_field('image');
                    $client= get_field('client');
                    $description= get_field('description');
                    $nom= get_field('nom');



                    ?>
                    

                <div class="item">
                    <div class="icon">,,</div>
                    <p><?php echo $description ?></p>
                    <div class="media">
                        <div class="author_img"><img src="<?php echo $image['url']?>" alt=""></div>
                        <div class="media-body author_description">
                            <a href="#">
                                <h5><?php echo $nom?></h5>
                            </a>
                            <h6><?php echo $client?></h6>
                        </div>
                    </div>
                </div>
                <!--<div class="item">
                    <div class="icon">,,</div>
                    <p>CabParis cras pardon you absolutely bladdered barmy gosh cobblers a load of old tosh brolly such
                        a fibber easy peasy so I said is, blatant pardon me plastered baking cakes Why James Bond cup of
                        char dropped a clanger he nicked it fantastic brown.!!</p>
                    <div class="media">
                        <div class="author_img"><img src="<?php echo get_template_directory_uri();?>/assets/img/author_img.jpg" alt=""></div>
                        <div class="media-body author_description">
                            <a href="#">
                                <h5>Rodney Artichoke</h5>
                            </a>
                            <h6>Lead designer</h6>
                        </div>
                    </div>
                </div>-->
                <?php
                        }
                        wp_reset_postdata();
            ?>
            <?php } ?>     
            </div>
        </div>
    </section>
    <?php get_footer()?>