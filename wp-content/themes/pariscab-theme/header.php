<!doctype html>
<html lang="en">


<!-- Mirrored from CabParis-html.pixelomatic.com/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 25 Jan 2023 11:20:42 GMT -->

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/assets/img/favicon.png">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/vendors/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/vendors/bootstrap-datepicker/jquery.mobile.datepicker.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/vendors/bootstrap-datepicker/jquery.mobile.datepicker.theme.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/vendors/elagent-icon/style.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/vendors/themfiy/themify-icons.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/vendors/simple-line-icon/simple-line-icons.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/vendors/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/vendors/slick/slick.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/vendors/slick/slick-theme.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/vendors/animation/animate.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/vendors/calender/dcalendar.picker.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/vendors/magnify-popup/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/responsive.css">
    <?php wp_head();?>
    <style>
        .menu-item{
            padding:10px;
        }
        .menu-item > a{
            color:#000;
            text-transform:uppercase;
        }
    </style>

    <title>Cab Paris</title>
</head>

<body>
    <div class="sampleContainer">
        <div class="loading">
            <img src="<?php echo get_template_directory_uri();?>/assets/img/loader_5.gif" alt="">
            <h2>Chargement
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </h2>
        </div>
    </div>

    <header class="header_area" style="z-index: 9999">
        <div class="container-fluid d-flex">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a href="/" class="logo"><img src="<?php echo get_template_directory_uri();?>/assets/img/logo_h.png" alt=""></a>
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="menu_toggle">
                        <span class="hamburger">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                        <span class="hamburger-cross">
                            <span></span>
                            <span></span>
                        </span>
                    </span>
                </button>
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    
                   
                    <?php
                        wp_nav_menu(array(
                            'theme_location' => 'primary',
                            'container' => false,
                            'menu_class' => 'nav navbar-nav menu mr-auto'
                        ));
                    ?>
                  
                    <!--
                    <ul class="nav navbar-nav menu mr-auto">
                        <li class="nav-item active">
                            <a  class="nav-link " href="/" role="button" aria-haspopup="true"
                                aria-expanded="false">
                                Acceuil
                        </li>
                        <li class="nav-item dropdown submenu"><a class="nav-link dropdown-toggle"
                                href="#">Aéroport ORLY/ROISSY CDG</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item"><a class="nav-link" href="/aeroport-roissy-orly">Taxi Paris ORLY</a></li>
                                <li class="nav-item"><a class="nav-link" href="/aeroport-roissy-orly">Taxi Paris ROISSY CDG</a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item"><a class="nav-link" href="/tarifs">Tarifs</a></li>
                        <li class="nav-item dropdown submenu"><a class="nav-link dropdown-toggle" href="#">Taxi Gares
                                Aéroport</a>
                            <ul class="dropdown-menu">
                                <li class="nav-item"><a class="nav-link" href="/gare-ostriz-orly">Gare OSTRIZ ->
                                            ORLY</a></li>
                                <li class="nav-item"><a class="nav-link" href="/gare-nord-orly">Gare Du Nord ->
                                            ORLY</a></li>
                                <li class="nav-item"><a class="nav-link" href="/gare-st-lazare">Gare StLazare ->
                                            ORLY</a></li>
                            </ul>
                        </li>

                        <li class="nav-item"><a class="nav-link" href="/blog">Blog</a></li>


                        <li class="nav-item"><a class="nav-link" href="/contactez-nous">Contactez-Nous</a></li>

                    </ul>
                    -->

            </nav>

            <div class="menu_btn" style="padding:10px;">
                <a href="/reservation" class="book_btn"><label style="font-size: small;">Réservation</label></a>
            </div>
            
        </div>
    </header>