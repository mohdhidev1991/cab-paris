<?php /*
 Template Name: Tarrifs
 */ ?>
<?php get_header()?>
<section class="city_location_area sec_pad">
        <div class="container">
            <div class="section_title text-center">
                <h5>Allons avec nous</h5>
                <h2>Nos tarifs municipaux</h2>
            </div>
            <div class="city_location_tab">
                <ul class="nav nav-tabs location_inner_tab" id="myTab2" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="taxi-tab" data-toggle="tab" href="#taxi" role="tab" aria-controls="taxi" aria-selected="true"><i class="flaticon-taxi"></i>Taxi Book</a>
                    </li>
                    
                    
                </ul>
                <div class="tab-content" id="myTabContent2">
                    <div class="tab-pane fade show active" id="taxi" role="tabpanel" aria-labelledby="taxi-tab">
                        <ul class="nav nav-tabs booking_quality_tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="one" aria-selected="true">Standard</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="two" aria-selected="false">Business</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="three" aria-selected="false">VIP</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="four-tab" data-toggle="tab" href="#four" role="tab" aria-controls="four" aria-selected="false">Comfort</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="five-tab" data-toggle="tab" href="#five" role="tab" aria-controls="five" aria-selected="false">Premium</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="six-tab" data-toggle="tab" href="#six" role="tab" aria-controls="six" aria-selected="false">Economy</a>
                            </li>
                        </ul>
                        <div class="tab-content boking_information">
                            <div class="tab-pane fade show active" id="one" role="tabpanel" aria-labelledby="one-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement à votre place.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi dans votre zone disponible</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="two" role="tabpanel" aria-labelledby="two-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="three" role="tabpanel" aria-labelledby="three-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="four" role="tabpanel" aria-labelledby="four-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="five" role="tabpanel" aria-labelledby="five-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Chauffeurs de taxi à louer dans votre ville</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="six" role="tabpanel" aria-labelledby="six-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="plan" role="tabpanel" aria-labelledby="plan-tab">
                        <ul class="nav nav-tabs booking_quality_tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="seven-tab" data-toggle="tab" href="#seven" role="tab" aria-controls="seven" aria-selected="true">Standard</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="eight-tab" data-toggle="tab" href="#eight" role="tab" aria-controls="eight" aria-selected="false">Business</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="nine-tab" data-toggle="tab" href="#nine" role="tab" aria-controls="nine" aria-selected="false">VIP</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="ten-tab" data-toggle="tab" href="#ten" role="tab" aria-controls="ten" aria-selected="false">Comfort</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="eleven-tab" data-toggle="tab" href="#eleven" role="tab" aria-controls="eleven" aria-selected="false">Premium</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="twelve-tab" data-toggle="tab" href="#twelve" role="tab" aria-controls="twelve" aria-selected="false">Economy</a>
                            </li>
                        </ul>
                        <div class="tab-content boking_information">
                            <div class="tab-pane fade show active" id="seven" role="tabpanel" aria-labelledby="seven-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="eight" role="tabpanel" aria-labelledby="eight-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nine" role="tabpanel" aria-labelledby="nine-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="ten" role="tabpanel" aria-labelledby="ten-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="eleven" role="tabpanel" aria-labelledby="eleven-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="twelve" role="tabpanel" aria-labelledby="twelve-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £1,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="bike" role="tabpanel" aria-labelledby="bike-tab">
                        <ul class="nav nav-tabs booking_quality_tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="thirteenth-tab" data-toggle="tab" href="#thirteenth" role="tab" aria-controls="thirteenth" aria-selected="true">Standard</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="fourteenth-tab" data-toggle="tab" href="#fourteenth" role="tab" aria-controls="fourteenth" aria-selected="false">Business</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="fifteenth-tab" data-toggle="tab" href="#fifteenth" role="tab" aria-controls="fifteenth" aria-selected="false">VIP</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="sixteenth-tab" data-toggle="tab" href="#sixteenth" role="tab" aria-controls="sixteenth" aria-selected="false">Comfort</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="seventeen-tab" data-toggle="tab" href="#seventeen" role="tab" aria-controls="seventeen" aria-selected="false">Premium</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="eightteen-tab" data-toggle="tab" href="#eightteen" role="tab" aria-controls="eightteen" aria-selected="false">Economy</a>
                            </li>
                        </ul>
                        <div class="tab-content boking_information">
                            <div class="tab-pane fade show active" id="thirteenth" role="tabpanel" aria-labelledby="thirteenth-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="fourteenth" role="tabpanel" aria-labelledby="fourteenth-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £0,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="fifteenth" role="tabpanel" aria-labelledby="fifteenth-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="sixteenth" role="tabpanel" aria-labelledby="sixteenth-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="seventeen" role="tabpanel" aria-labelledby="seventeen-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="eightteen" role="tabpanel" aria-labelledby="eightteen-tab">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £6<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Taxi drivers for hire in your city</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi à votre place</h4>
                                                <p>Nouvelle F.A.Q. plus grande et meilleure section.</p>
                                            </div>
                                            <div class="mile">
                                                £1.3<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Choisissez la position de la publicité</h4>
                                                <p>Venez dans notre entrepôt et choisissez votre article.</p>
                                            </div>
                                            <div class="mile">
                                                £8<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Comment obtenir une remise avec une application mobile</h4>
                                                <p>Rappelez-vous quand l'avez-vous perdu.</p>
                                            </div>
                                            <div class="mile">
                                                £2,4<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="media location-milage">
                                            <div class="media-body">
                                                <h4>Obtenez le meilleur prix de taxi chez vous</h4>
                                                <p>Nous vous amènerons rapidement et confortablement dans votre ville.</p>
                                            </div>
                                            <div class="mile">
                                                £4,2<span>/ km</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    
    
    


<?php get_footer()?>