
<?php /* Template Name: Article Page*/ ?>
<?php get_header();?>
		<!-- End Header -->

		<!-- ticker-news-section
			================================================== -->
		<!-- End ticker-news-section -->

		<!-- block-wrapper-section
			================================================== -->
			<?php	
				$p_art = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			?>
			<?php
                     $the_query = new WP_Query(array("post_type" => "Post", "orderby" => "date", "order" => "DESC",'posts_per_page' => '100',));

            ?>

			<?php if ($the_query->have_posts()) { ?>

			<?php
			while ($the_query->have_posts()) {

			$the_query->the_post();



			$id_article = get_the_ID();
			$url_post = get_permalink($id_article); 


			$title = get_the_title();

			$content = get_the_content();
			$image = get_field('image');
			$date = get_the_date();
			?>

			<?php if(str_contains($url_post , $p_art)) { ?>
			<section class="blog_area sec_pad">
					<div class="container">
						<div class="row blog_inner">
							<div class="col-lg-12">
								<div class="main_blog_inner blog_single_inner">
									<article class="blog_item">
										<div class="blog_img">
											<a href="#" class="overlay"><img src="<?php echo $image['url'] ;?>" alt=""></a>
										</div>
										<div class="blog_content">
											<ul class="list-unstyled post_info">
												<li><a href="#"><?php echo 	$date ;  ?></a></li>
											</ul>
											<h2 class="blog_title"><?php echo $title ; ?></h2>
											<p><?php echo 	$content ;  ?></p>
											
										</div>
									</article>
									
								</div>
							</div>
							
						</div>
					</div>
				</section>

				<?php } ?>
                <?php
				}
				wp_reset_postdata();
				?>
                <?php } ?>
		 
		<!-- End block-wrapper-section -->

		<!-- footer 
			================================================== -->
		<?php get_footer();?>