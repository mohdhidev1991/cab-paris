<?php /*
 Template Name: GarenordOrly
 */ ?>
<?php get_header()?>

<section class="slider_area d-flex align-items-center">
    <section class="booking_form_area bg_one">
        <div class="container">
                <div class="booking_slider slick">

                <div class="booking_form_info two "style="width: min-content;">
                    <div class="tab_img">
                        
                    <div class="boking_content">
                        <h1> Forfaits</h1>
                        Combien coûte un taxi entre Gare austerlitz et l'Aéroport Orly ?
                        <table style="align-content: center; align-items: center; width:100% ">
                            <tr>
                                <td>Gare de Nord</td>
                                <td>Orly Sud</td>
                                <td>42 £</td>
                            </tr>
                            <tr>
                                <td>Gare De Nord</td>
                                <td>Orly Ouest</td>
                                <td>42 £</td>
                            </tr>
                        </table>

                        <div class="col-lg-12">
                                    <div class="form-group">
                                        <a href="/reservation" class="btn slider_btn dark_hover">Réservez maintenant!</a>
                                    </div>
                        </div>

                    </div>
                </div>
                
                
                </div>
                <div>

                    <div class="booking_form_info two">
                        <div class="tab_img">
                            
                        <div >
                            <h1> Paris</h1>


                            <table style="align-content: center; align-items: center;width:100% ">
                                <tr>
                                    <td rowspan="12">paris rive droite</td>
                                    <td>Paris 1ére</td>
                                </tr>
                                <tr>
                                    <td>Paris 2éme</td>
                                </tr>
                                <tr>
                                    <td>Paris 3éme</td>
                                </tr>
                                <tr>
                                    <td>Paris 4éme</td>
                                </tr>
                                <tr>
                                    <td>Paris 8éme</td>
                                </tr>
                                <tr>
                                    <td>Paris 9éme</td>
                                </tr>
                                <tr>
                                    <td>Paris 10éme</td>
                                </tr>
                                <tr>
                                    <td>Paris 11éme</td>
                                </tr>
                                <tr>
                                    <td>Paris 16éme</td>
                                </tr>
                                <tr>
                                    <td>Paris 17éme</td>
                                </tr>
                                <tr>
                                    <td>Paris 18éme</td>
                                </tr>
                                <tr>
                                    <td>Paris 19éme</td>
                                </tr>
                                <tr>
                                    <td rowspan="7">Paris Rive Gauche</td>
                                </tr>
                                <tr>
                                    <td>Paris 5éme</td>
                                </tr>
                                <tr>
                                    <td>Paris 6éme</td>
                                </tr>
                                <tr>
                                    <td>Paris 7éme</td>
                                </tr>
                                <tr>
                                    <td>Paris 13éme</td>
                                </tr>
                                <tr>
                                    <td>Paris 14éme</td>
                                </tr>
                                <tr>
                                    <td>Paris 15éme</td>
                                </tr>
                            </table>

                            <div class="col-lg-12">
                                    <div class="form-group">
                                        <a href="/reservation" class="btn slider_btn dark_hover">Réservez maintenant!</a>
                                    </div>
                            </div>

                            
                        </div>
                        
                    </div>
                    
                    
                    </div>

            </div>
            




        </div>

       
        
        </div>

        
        
    </section>
    
    </section>
<?php get_footer()?>