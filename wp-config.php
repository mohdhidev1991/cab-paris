<?php
/**
 * إعدادات ووردبريس الأساسية
 *
 * عملية إنشاء الملف wp-config.php تستخدم هذا الملف أثناء التنصيب. لا يجب عليك
 * استخدام الموقع، يمكنك نسخ هذا الملف إلى "wp-config.php" وبعدها ملئ القيم المطلوبة.
 *
 * هذا الملف يحتوي على هذه الإعدادات:
 *
 * * إعدادات قاعدة البيانات
 * * مفاتيح الأمان
 * * بادئة جداول قاعدة البيانات
 * * المسار المطلق لمجلد الووردبريس
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** إعدادات قاعدة البيانات - يمكنك الحصول على هذه المعلومات من مستضيفك ** //

/** اسم قاعدة البيانات لووردبريس */
define( 'DB_NAME', 'taxismlcabparis' );

/** اسم مستخدم قاعدة البيانات */
define( 'DB_USER', 'taxismlcabparis' );

/** كلمة مرور قاعدة البيانات */
define( 'DB_PASSWORD', 'AxB612670433242' );

/** عنوان خادم قاعدة البيانات */
define( 'DB_HOST', 'taxismlcabparis.mysql.db' );

/** ترميز قاعدة البيانات */
define( 'DB_CHARSET', 'utf8mb4' );

/** نوع تجميع قاعدة البيانات. لا تغير هذا إن كنت غير متأكد */
define( 'DB_COLLATE', '' );

/**#@+
 * مفاتيح الأمان.
 *
 * تغيير هذه العبارات إلى عبارات فريدة مختلفة!
 * استخدم الرابط التالي لتوليد المفاتيح {@link https://api.wordpress.org/secret-key/1.1/salt/}
 * يمكنك تغيير هذه في أي وقت لإلغاء جميع ملفات تعريف الارتباط الموجودة. سيؤدي هذا إلى إجبار جميع المستخدمين على تسجيل الدخول مرة أخرى.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'qrZLKg=DG7t ;{eE]50Gt}W.ffe<yI>5><,G.sX|lwk_:N_k=+_g2S@cumTd+g J' );
define( 'SECURE_AUTH_KEY',  'c_}x.3UWBgo.YW$*:=F2u1eQj5tV8~ds /%csi?^yoCtzirt8iwrU<i*{s$2QI;p' );
define( 'LOGGED_IN_KEY',    ')AZt~9/~PU%A4aUB26b>-9WzqTN~8$8Zx$r=FnffKSX:DYkSG#h1A8>k#2[e~X#$' );
define( 'NONCE_KEY',        '5RB*+J@7B6dc^6&os{E?C<LIW*Kn`M6n8wa(do;)6l{e-DWUOO=zq|c09k~1ArV]' );
define( 'AUTH_SALT',        'rv~cT5bj3Li_(f,7X[.>]4nt{$F~>TH{<O<KxL&y;&wjkdRetG.shrc,LS&Y$qLO' );
define( 'SECURE_AUTH_SALT', 'VBKUsn`Zw6bY[)iz%f7<.*F|4{DN)Y@e.-0LiToXR&oh}0MP5RX}|0qR&<!H8*Wq' );
define( 'LOGGED_IN_SALT',   '3YK6&@Auh<rfzU%);Ef7a)b1$CMw9Hl+vN ]w+Al46DzC<Hc1gsadQT 7[[Vq@c!' );
define( 'NONCE_SALT',       'tNg:/|Lf3c(Q03gSrbrPH H|UCi.@CWKL.kh0x>4OZVML]o TUfP<C%K-4e0C Bp' );

/**#@-*/

/**
 * بادئة الجداول في قاعدة البيانات.
 *
 * تستطيع تركيب أكثر من موقع على نفس قاعدة البيانات إذا أعطيت لكل موقع بادئة جداول مختلفة
 * يرجى استخدام حروف، أرقام وخطوط سفلية فقط!
 */
$table_prefix = 'wp_';

/**
 * للمطورين: نظام تشخيص الأخطاء
 *
 * قم بتغييرالقيمة، إن أردت تمكين عرض الملاحظات والأخطاء أثناء التطوير.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* هذا هو المطلوب، توقف عن التعديل! نتمنى لك التوفيق. */

/** المسار المطلق لمجلد ووردبريس. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** إعداد متغيرات الووردبريس وتضمين الملفات. */
require_once ABSPATH . 'wp-settings.php';
